package sensorManager;

import org.json.JSONObject;

public interface JSONSensorData {

    public JSONObject getData();

    public String getSensorName();
}
