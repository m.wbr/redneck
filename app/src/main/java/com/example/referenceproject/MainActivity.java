package com.example.referenceproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import strategies.EnergyStrategySensorBased;
import strategies.ReportingStrategyDistanceBased;
import strategies.ReportingStrategyTimeBased;
import utility.MainActivityHelper;
import locationTracker.GPSFusedLocationTracker;
import locationTracker.GPSLocationTracker;
import sensorManager.JSONSensorDataManager;
import sensors.Accelerometer;
import sensors.Gyroscope;
import sensors.Magnetometer;
import sensors.Sensor;
import strategies.EnergyStrategy;
import strategies.EnergyStrategyDistanceBased;
import strategies.ReportingStrategy;

public class MainActivity extends AppCompatActivity {

    Button activateSensorsButton;
    Button deactivateSensorsButton;;
    Button getSensorDataButton;

    Button activateGPSButton;
    Button activateGPSWithNetworkButton;
    Button deactivateGPSButton;

    Button activateFusedGPSHighButton;
    Button activateFusedGPSNormalButton;
    Button deactivateFusedGPSButton;

    Button aOnButton;
    Button aOffButton;

    Button bOnButton;
    Button bOffButton;

    Button b1OnButton;
    Button b1OffButton;

    Button b2OnButton;
    Button b2OffButton;

    public TextView gpsTextView;
    public TextView strategyTextView;

    private boolean keepRunning = false;

    private JSONSensorDataManager sensorDataManager;

    private Sensor accelerometer;
    private Sensor gyroscope;
    private Sensor magnetometer;
    private GPSLocationTracker gps;
    private GPSFusedLocationTracker gpsFused;
    private EnergyStrategy energyStrategy;
    private ReportingStrategy reportingStrategy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivityHelper.setMainActivity(this);

        activateSensorsButton = (Button) findViewById(R.id.activateSensorsButton);
        deactivateSensorsButton = (Button) findViewById(R.id.deactivateSensorsButton);
        getSensorDataButton = (Button) findViewById(R.id.getSensorDataButton);

        activateGPSButton = (Button) findViewById(R.id.gpsOnButton);
        activateGPSWithNetworkButton = (Button) findViewById(R.id.gpsWithNetworkOnButton);
        deactivateGPSButton = (Button) findViewById(R.id.gpsOffButton);

        aOnButton = (Button) findViewById(R.id.aOnButton);
        aOffButton = (Button) findViewById(R.id.aOffButton);

        bOnButton = (Button) findViewById(R.id.bOnButton);
        bOffButton = (Button) findViewById(R.id.bOffButton);

        b1OnButton = (Button) findViewById(R.id.b1OnButton);
        b1OffButton = (Button) findViewById(R.id.b1OffButton);

        b2OnButton = (Button) findViewById(R.id.b2OnButton);
        b2OffButton = (Button) findViewById(R.id.b2OffButton);

        gpsTextView = (TextView) findViewById(R.id.gpsTextView);
        strategyTextView = (TextView) findViewById(R.id.strategyTextView);

        // create manager
        sensorDataManager = new JSONSensorDataManager(this);

        // create sensors
        accelerometer = new Accelerometer(MainActivity.this);
        gyroscope = new Gyroscope(MainActivity.this);
        magnetometer = new Magnetometer(MainActivity.this);

        // create GPS
        gps = new GPSLocationTracker(MainActivity.this);
        gpsFused = new GPSFusedLocationTracker((MainActivity.this));

        // strategy stuff
        //energyStrategy = new EnergyStrategyDistanceBased(this);
        //reportingStrategy = new ReportingStrategyDistanceBased();

        //gps.setEnergyStrategy(energyStrategy);

        // Button logic

        /////////////////////////////// STRATEGY BUTTONS ////////////////////////////////

        aOnButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ReportingStrategyTimeBased reportingStrategyTimeBased = new ReportingStrategyTimeBased();
                reportingStrategyTimeBased.setTimeThreshold(1);
                reportingStrategy = reportingStrategyTimeBased;

                gps.setEnergyStrategy(null);
                gps.setReportingStrategy(null);
                gps.setReportingStrategy(reportingStrategy);

                reportingStrategy.enable();
                Toast.makeText(MainActivity.this.getApplicationContext(), "strategy enabled", Toast.LENGTH_SHORT).show();
            }
        });

        aOffButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                reportingStrategy.disable();

                gps.setEnergyStrategy(null);
                gps.setReportingStrategy(null);

                Toast.makeText(MainActivity.this.getApplicationContext(), "strategy disabled", Toast.LENGTH_SHORT).show();
            }
        });

        bOnButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ReportingStrategyDistanceBased reportingStrategyDistanceBased = new ReportingStrategyDistanceBased();
                reportingStrategyDistanceBased.setDistanceThreshold(50);
                reportingStrategy = reportingStrategyDistanceBased;

                gps.setEnergyStrategy(null);
                gps.setReportingStrategy(null);
                gps.setReportingStrategy(reportingStrategy);

                reportingStrategy.enable();

                Toast.makeText(MainActivity.this.getApplicationContext(), "strategy enabled", Toast.LENGTH_SHORT).show();
            }
        });

        bOffButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                reportingStrategy.disable();

                gps.setEnergyStrategy(null);
                gps.setReportingStrategy(null);

                Toast.makeText(MainActivity.this.getApplicationContext(), "strategy disabled", Toast.LENGTH_SHORT).show();
            }
        });

        b1OnButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ReportingStrategyDistanceBased reportingStrategyDistanceBased = new ReportingStrategyDistanceBased();
                reportingStrategyDistanceBased.setDistanceThreshold(50);
                reportingStrategy = reportingStrategyDistanceBased;

                EnergyStrategyDistanceBased energyStrategyDistanceBased = new EnergyStrategyDistanceBased(MainActivity.this);
                energyStrategyDistanceBased.setDistanceThreshold(50);
                energyStrategyDistanceBased.setMaxVelocity(2);
                energyStrategy = energyStrategyDistanceBased;

                gps.setEnergyStrategy(null);
                gps.setReportingStrategy(null);
                gps.setReportingStrategy(reportingStrategy);
                gps.setEnergyStrategy(energyStrategy);

                reportingStrategy.enable();
                energyStrategy.enable();

                Toast.makeText(MainActivity.this.getApplicationContext(), "strategy enabled", Toast.LENGTH_SHORT).show();
            }
        });

        b1OffButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                reportingStrategy.disable();
                energyStrategy.disable();

                gps.setEnergyStrategy(null);
                gps.setReportingStrategy(null);

                Toast.makeText(MainActivity.this.getApplicationContext(), "strategy disabled", Toast.LENGTH_SHORT).show();
            }
        });

        b2OnButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ReportingStrategyDistanceBased reportingStrategyDistanceBased = new ReportingStrategyDistanceBased();
                reportingStrategyDistanceBased.setDistanceThreshold(50);
                reportingStrategy = reportingStrategyDistanceBased;

                EnergyStrategySensorBased energyStrategySensorBased = new EnergyStrategySensorBased(accelerometer);
                energyStrategySensorBased.setAccelarationThreshold(2);
                energyStrategy = energyStrategySensorBased;

                gps.setEnergyStrategy(null);
                gps.setReportingStrategy(null);
                gps.setReportingStrategy(reportingStrategy);
                gps.setEnergyStrategy(energyStrategy);

                reportingStrategy.enable();
                energyStrategy.enable();

                Toast.makeText(MainActivity.this.getApplicationContext(), "strategy enabled", Toast.LENGTH_SHORT).show();
            }
        });

        b2OffButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                reportingStrategy.disable();
                energyStrategy.disable();

                gps.setEnergyStrategy(null);
                gps.setReportingStrategy(null);

                Toast.makeText(MainActivity.this.getApplicationContext(), "strategy disabled", Toast.LENGTH_SHORT).show();
            }
        });

        /////////////////////////////// GPS BUTTONS ////////////////////////////////

        activateGPSButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                gps.startTracking();
            }
        });

        activateGPSWithNetworkButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                gps.startTrackingWithNetwork();
            }
        });

        deactivateGPSButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                gps.stopTracking();
            }
        });

        /*
        activateFusedGPSHighButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                gpsFused.setHighAccuracy();
                gpsFused.startTracking();
            }
        });
         */

        /////////////////////////////// SENSOR BUTTONS ////////////////////////////////

        getSensorDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        activateSensorsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                accelerometer.start();

            }
        });

        deactivateSensorsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                accelerometer.stop();
            }
        });

    }
}
