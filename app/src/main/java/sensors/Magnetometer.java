package sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.util.Log;

import com.example.referenceproject.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import strategies.EnergyStrategy;

public class Magnetometer extends sensors.Sensor {

    public Magnetometer(MainActivity mainActivity) {

        super(mainActivity, Sensor.TYPE_MAGNETIC_FIELD, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected JSONObject createJSON(SensorEvent event) {

        /* vielleicht auf Himmelsrichtung oder Grad umrechnen
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        float summe = x + y;
        */

        JSONObject data = new JSONObject();

        try {
            data.put("x",  event.values[0]);
            data.put("y",  event.values[1]);
            data.put("z",  event.values[2]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }

    @Override
    public String getSensorName() {
        return "Magnetometer";
    }
}
