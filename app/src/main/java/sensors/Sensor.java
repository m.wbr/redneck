package sensors;

import android.content.Context;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.example.referenceproject.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import sensorManager.JSONSensorData;
import strategies.EnergyStrategy;

public abstract class Sensor implements JSONSensorData {

    private SensorManager sensorManager;
    private SensorEventListener sensorEventListener;
    private JSONObject data;
    private int sensorType;
    private int sensorDelay;
    private boolean sensorIsActive;
    private MainActivity mainActivity;
    protected EnergyStrategy energyStrategy;

    protected Sensor(MainActivity mainActivity, int sensorType, int sensorDelay){

        this.sensorType = sensorType;
        this.sensorDelay = sensorDelay;
        this.sensorIsActive = false;
        this.mainActivity = mainActivity;
        this.init();
    }

    public void setEnergyStrategy(EnergyStrategy energyStrategy){
        this.energyStrategy = energyStrategy;
    }

    protected abstract JSONObject createJSON(SensorEvent event);

    private void init(){

        sensorManager = (SensorManager) mainActivity.getSystemService(Context.SENSOR_SERVICE);

        sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {

                if(event.sensor.getType() == sensorType){

                    data = createJSON(event);

                    if(energyStrategy != null) energyStrategy.onSensorChanged(data);
                }
            }

            @Override
            public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {

            }
        };
    }

    /**
     * Sensor starts to generate output data
     */
    public void start(){

        if(sensorIsActive) return;
        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(sensorType), sensorDelay);
        sensorIsActive = true;
    }

    /**
     * Sensor stops to generate output data
     */
    public void stop(){

        if(!sensorIsActive) return;
        sensorManager.unregisterListener(sensorEventListener);
        sensorIsActive = false;
    }

    public boolean isActive(){
        return sensorIsActive;
    }

    /**
     * returns sensor data as JSONObject
     * @return JSONObject
     */
    @Override
    public JSONObject getData(){

        try {
            if(!sensorIsActive) throw new Exception();
        }catch (Exception e){
            Log.e("Sensor class", "SENSOR NOT ACTIVATED");
        }

        try {
            if(this.data == null) throw new Exception();
        }catch (Exception e){
            Log.e("Sensor class", "DATA == NULL");
        }

        return this.data;
    }
}
