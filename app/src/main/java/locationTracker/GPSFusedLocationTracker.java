package locationTracker;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.example.referenceproject.MainActivity;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import utility.FileHelper;
import utility.TimestampCreator;
import sensorManager.JSONSensorData;

public class GPSFusedLocationTracker implements LocationListener, JSONSensorData {

    public LocationRequest locationRequest;
    private long interval = 2000;
    private long fastestInterval = 1000;
    private int accuracy = LocationRequest.PRIORITY_HIGH_ACCURACY;
    private JSONObject data;
    private MainActivity mainActivity;
    private LocationCallback callback;
    private Location location;
    private boolean debug = false;

    public GPSFusedLocationTracker(MainActivity mainActivity){

        this.mainActivity = mainActivity;
        locationRequest = new LocationRequest();
        locationRequest.setPriority(this.accuracy);
        locationRequest.setInterval(this.interval);
        locationRequest.setFastestInterval(this.fastestInterval);

        callback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                onLocationChanged(locationResult.getLastLocation());
            }
        };

        //Toast.makeText(mainActivity.getApplicationContext(), "STARTING", Toast.LENGTH_LONG).show();
    }

    @Override // LocationListener
    public void onLocationChanged(Location location){

        this.location = location;

        //Toast.makeText(mainActivity.getApplicationContext(), "locationChanged", Toast.LENGTH_LONG).show();
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        double alt = location.getAltitude();

        int acc = locationRequest.getPriority();

        String text = "fusedGPS\n" + "Latitude: " + lat + "\n" + "Longitude: " + lon + "\n" + "Altitude " + alt + "\n" + "Accuracy" + acc;
        mainActivity.gpsTextView.setText(text);

        JSONObject jsonData = this.getData();
        try {
            jsonData.put("time", TimestampCreator.createTimestampString());
            jsonData.put("accuracy", acc);
            jsonData.put("type:", "fusedGPS");
        }catch (JSONException e){
            Log.e("JSON EXCEPTION: ", String.valueOf(e));
        }
        String data = jsonData.toString();

        FileHelper.saveToFile(data + ",");
    }

    @Override // LocationListener
    public void onStatusChanged(String provider, int status, Bundle extras) {
        if(debug) Toast.makeText(mainActivity.getApplicationContext(), provider.toUpperCase() + " HAS CHANGED\n" + "STATUS: " + status, Toast.LENGTH_LONG).show();
    }

    @Override // LocationListener
    public void onProviderEnabled(String provider) {
        Toast.makeText(mainActivity.getApplicationContext(), "Please Activate " + provider.toUpperCase(), Toast.LENGTH_LONG).show();
    }

    @Override // LocationListener
    public void onProviderDisabled(String provider) {
        if(debug) Toast.makeText(mainActivity.getApplicationContext(), provider.toUpperCase() + " ENABLED", Toast.LENGTH_LONG).show();
    }

    @Override // JSONSensorData
    public JSONObject getData() {

        data = new JSONObject();

        if(location == null){

            if(debug) Toast.makeText(mainActivity.getApplicationContext(), "gps location == null", Toast.LENGTH_LONG).show();
            try {
                data.put("longitude", 0);
                data.put("latitude", 0);
                data.put("altitude", 0);
            }catch (JSONException e){
                if(debug) Toast.makeText(mainActivity.getApplicationContext(), "JSONException GPSLocationManager", Toast.LENGTH_LONG).show();
                Log.e("JSON EXCEPTION: ", String.valueOf(e));
            }
        }else{

            try {
                data.put("longitude", location.getLongitude());
                data.put("latitude", location.getLatitude());
                data.put("altitude", location.getAltitude());
            }catch (JSONException e){
                if(debug) Toast.makeText(mainActivity.getApplicationContext(), "JSONException GPSLocationManger", Toast.LENGTH_LONG).show();
                Log.e("JSON EXCEPTION: ", String.valueOf(e));
            }
        }

        return data;
    }

    @Override // JSONSensorData
    public String getSensorName() {
        return "GPS";
    }

    public void startTracking(){
        Toast.makeText(mainActivity.getApplicationContext(), "starting fusedGPS", Toast.LENGTH_LONG).show();
        LocationServices.getFusedLocationProviderClient(this.mainActivity).requestLocationUpdates(locationRequest, callback, Looper.myLooper());
    }

    public void stopTracking(){
        Toast.makeText(mainActivity.getApplicationContext(), "stopping fusedGPS", Toast.LENGTH_LONG).show();
        LocationServices.getFusedLocationProviderClient(this.mainActivity).removeLocationUpdates(callback);
    }

    public void setHighAccuracy(){
        Toast.makeText(mainActivity.getApplicationContext(), "high accuracy", Toast.LENGTH_LONG).show();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void setBalancedAccuracy(){
        Toast.makeText(mainActivity.getApplicationContext(), "balanced accuracy", Toast.LENGTH_LONG).show();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    public void setLowAccuracy(){
        locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
    }

    public void setLowestAccuracy(){
        locationRequest.setPriority(LocationRequest.PRIORITY_NO_POWER);
    }

    public void setInterval(long interval){
        locationRequest.setInterval(interval);
    }

    public void setFastestInterval(long interval){
        locationRequest.setFastestInterval(interval);
    }
}
