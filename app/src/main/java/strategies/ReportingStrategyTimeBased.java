package strategies;

import android.location.Location;
import android.util.Log;
import org.json.JSONObject;

import utility.JSONUtility;

public class ReportingStrategyTimeBased extends ReportingStrategy {

    private boolean debug = true;

    // Time based strategy
    private double time = -1;
    private long timeThreshold = 1;             // in seconds
    private double timeDifference;

    // check time-threshold for reporting-strategy
    private boolean timeThresholdExceeded(){

        double newTime = (double)System.currentTimeMillis()/1000;
        if(this.time == -1) this.time = newTime;
        this.timeDifference = newTime - this.time;

        if(debug) Log.e("ReportingStrategy", "TimeBased: deltaTime = " + time);

        if(timeDifference < this.timeThreshold){
            return false;
        }else{
            this.time = newTime;
            return true;
        }
    }

    /**
     * set time-intervall for GPS-fixes
     * @param seconds: long
     */
    public void setTimeThreshold(long seconds) {
        this.timeThreshold = seconds;
    }

    /**
     * get time-intervall for GPS-fixes
     * @return time in seconds: long
     */
    public long getTimeThreshold() {
        return this.timeThreshold;
    }

    @Override // ReportingStrategy
    public void onLocationChanged(Location location) {

        if(!isEnabled) return;

        countGPSFix();

        if(timeThresholdExceeded()){

            JSONObject data = JSONUtility.toJSONObject(location);

            this.onThresholdExceeded(data);
        }
    }

    @Override // Strategy
    public void enable() {
        this.isEnabled = true;
    }

    @Override // Strategy
    public void disable() {
        this.isEnabled = false;
    }
}

