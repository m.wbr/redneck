package strategies;

import org.json.JSONObject;

import locationTracker.GPSLocationTracker;

public abstract class EnergyStrategy extends Strategy {

    protected boolean initialStateGPS;                       // used to set back the initial state of gps after energy-strategy is disabled
    protected boolean initialStateGPSwithNetwork;            // used to set back the initial state of gps after energy-strategy is disabled

    public void setGPS(GPSLocationTracker gps){
        this.gps = gps;
    }

    protected GPSLocationTracker gps;

    public abstract void onSensorChanged(JSONObject data);

    // set
    protected void resetGPSToInitialState(){

        if(initialStateGPS == gps.isActive()) return;

        if(initialStateGPS == false){
            gps.stopTracking();
            return;
        }

        if(initialStateGPSwithNetwork == true){
            gps.startTrackingWithNetwork();
            return;
        }

        gps.startTracking();

    }
}
