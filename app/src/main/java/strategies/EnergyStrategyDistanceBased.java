package strategies;

import android.location.Location;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.example.referenceproject.MainActivity;

import org.json.JSONObject;

import utility.MainActivityHelper;


public class EnergyStrategyDistanceBased extends EnergyStrategy {

    // debug stuff
    private boolean debug = true;
    private int counter = 0;

    // distance based
    private Location oldLocation;
    private float estimatedDistance;
    private float distanceBetweenFixes;
    private float initialDistanceThreshold = 5;
    private float distanceThreshold;
    private double velocity;                    // in meter/s
    private double maxVelocity = 2;             // in meter/s
    private double time = -1;
    private double deltaTime;
    private double timeAtLastFix = -1;
    private double timeBetweenFixes = 0;
    private boolean energyPlanIsActive = false;

    // safety timeout threshold
    private boolean timeoutIsEnabled = false;
    private int timeoutCounter = 0;
    private int timeoutLimit = 10;

    // thread stuff
    private MainActivity mainActivity;
    private Thread thread;
    private final int SLEEPINTERVAL = 1000;      // in milliseconds

    public EnergyStrategyDistanceBased(MainActivity mainActivity){

        this.mainActivity = mainActivity;
        reset();
    }

    private void reset(){
        timeoutCounter = 0;
        estimatedDistance = 0;
        distanceThreshold = initialDistanceThreshold;
    }

    @Override // EnergyStrategy
    public void onSensorChanged(JSONObject data) {
        // nothing to do yet
    }

    @Override // EnergyStrategy
    public void onLocationChanged(Location location) {

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: onLocationChanged()");

        if(!isEnabled) return;

        calculateDeltaTime();
        calculateDistanceBetweenFixes(location);
        calculateTimeBetweenFixes();
        calculateVelocity();
        calculateEnergyPlan();
    }

    private void calculateDeltaTime(){

        double newTime = (double)System.currentTimeMillis()/1000;
        if(this.time == -1){
            deltaTime = 0;
        }else{
            deltaTime =  newTime - time;
        }
        time = newTime;

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: calculateDeltaTime() = " + deltaTime);
    }

    private void calculateDistanceBetweenFixes(Location location){

        Location newLocation = location;
        if(oldLocation == null){
            distanceBetweenFixes = 0;
        }else{
            distanceBetweenFixes = this.oldLocation.distanceTo(newLocation);

            if(this.debug) Log.e("EnergyStrategy", "DistanceBased: calculateDistanceBetweenFixes() = " + distanceBetweenFixes);

            // if distance between new gps-fix and last gps-fix is still under distance-threshold, set distance-threshold to the missing distance
            if(distanceBetweenFixes < distanceThreshold) distanceThreshold = initialDistanceThreshold - distanceBetweenFixes;
        }
        oldLocation = newLocation;

    }

    private void calculateTimeBetweenFixes(){

        double timeAtNewFix = (double)System.currentTimeMillis()/1000;

        if(timeAtLastFix != -1) timeBetweenFixes = timeAtNewFix - timeAtLastFix;

        timeAtLastFix = timeAtNewFix;

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: calculateTimeBetweenFixes() = " + timeBetweenFixes);
    }

    private void calculateVelocity(){

        if(timeBetweenFixes == 0 || distanceBetweenFixes == 0){
            velocity = 0;
        }else{
            velocity = distanceBetweenFixes / timeBetweenFixes;
        }
        if(velocity > maxVelocity) velocity = maxVelocity;

        if(debug) Log.e("EnergyStrategy", "calculateVelocity() = " + velocity);
    }

    private void calculateEnergyPlan(){

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: calculateEnergyPlan().start");

        if(distanceBetweenFixes == 0) return;
        if(velocity == 0) return;
        if(deltaTime == 0) return;
        if(timeBetweenFixes == 0) return;

        startEnergyPlan();

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: calculateEnergyPlan().end");
    }

    private void startEnergyPlan(){

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: startEnergyPlan()");

        if(thread == null || !thread.isAlive()) {
            thread = new Thread() {
                @Override
                public void run() {

                    Looper.prepare();

                    energyPlanIsActive = true;
                    gps.stopTracking();

                    // while estimated distance under threshold
                    while (energyPlanIsActive) {

                        try {
                            sleep(SLEEPINTERVAL);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (debug) Log.e("Thread running?", String.valueOf(thread.isAlive()));

                        // for debugging
                        mainActivity.runOnUiThread(showEnergyPlan);

                        processEnergyPlan();

                    }

                    // when threshold exceeded
                    mainActivity.runOnUiThread(terminateEnergyPlan);
                    if (debug) Log.e("Thread running?", String.valueOf(thread.isAlive()));
                }
            };
            thread.start();
        }
    }

    private void processEnergyPlan(){

        calculateDeltaTime();
        calculateEstimatedDistance();

        if(debug) Log.e("EnergyStrategy", "velocity: " + velocity);
        if(debug) Log.e("EnergyStrategy", "velocity*deltaTime: " + (velocity * deltaTime));
        if(debug) Log.e("EnergyStrategy", "deltaTime: " + deltaTime);
        if(debug) Log.e("EnergyStrategy", "estimatedDistance: " + estimatedDistance);
        if(debug) Log.e("EnergyStrategy", "distanceThreshold: " + distanceThreshold);

        // safety threshold in case estimation takes too long
        if(timeoutIsEnabled){
            timeoutCounter++;
            if(debug) Log.e("EnergyStrategy", "DistanceBased: timeoutCounter = " + timeoutCounter);
            if(debug) Log.e("EnergyStrategy", "DistanceBased: timeoutLimit = " + timeoutLimit);
            if(timeoutCounter >= timeoutLimit) energyPlanIsActive = false;
        }

        if(!isEnabled) energyPlanIsActive = false;
        if(estimatedDistance >= distanceThreshold) energyPlanIsActive = false;
    }


    // for debugging
    private Runnable showEnergyPlan = new Runnable() {
        @Override
        public void run() {

            String text = "Energy Strategy"
                    + "\nvelocity = " + velocity
                    + "\nvelocity*deltaTime: " + (velocity * deltaTime)
                    + "\nmaxVelocity = " + maxVelocity
                    + "\nestimatedDistance = " + estimatedDistance
                    + "\ndistanceThreshold = " + distanceThreshold
                    + "\ndistance last fix = " + distanceBetweenFixes
                    + "\ninitialDistanceThreshold = " + initialDistanceThreshold
                    + "\nTimeDifference = " + deltaTime
                    + "\nTimeBetweenFixes = " + timeBetweenFixes
                    + "\ncounter = " + counter
                    + "\ntimeoutCounter = " + timeoutCounter
                    + "\ntimeoutLimit = " + timeoutLimit;

            // set text view
            MainActivityHelper.getMainActivity().strategyTextView.setText(text);
        }
    };


    private void calculateEstimatedDistance(){
        estimatedDistance += velocity * deltaTime;
    }

    private Runnable terminateEnergyPlan = new Runnable() {
        @Override
        public void run() {

            if(thread != null){

                if(debug) Log.e("EnergyStrategy", "DistanceBased: stopEnergyPlan()");
                if(debug) Log.e("stopEnergyPlan: Thread", String.valueOf(thread.isAlive()));

                try {
                    thread.join();
                    if(debug) Log.e("afterJoin: Thread", String.valueOf(thread.isAlive()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                thread.interrupt();

                if(debug) Log.e("afterInterrupt: Thread", String.valueOf(thread.isAlive()));
            }

            // counter for debugging
            counter++;

            // reset variables that need to be reset
            reset();

            if(!isEnabled) return;

            startGPS();
        }
    };

    private void startGPS(){

        if(gps.isActive()) return;

        if(initialStateGPSwithNetwork){
            gps.startTrackingWithNetwork();
        }else{
            gps.startTracking();
        }
    }

    /**
     *
     * @param distanceThreshold in meter
     */
    public void setDistanceThreshold(float distanceThreshold){
        this.initialDistanceThreshold = distanceThreshold;
    }

    public void setMaxVelocity(double maxVelocity){
        this.maxVelocity = maxVelocity;
    }

    public void enableTimeoutLimit(){
        this.timeoutIsEnabled = true;
    }

    public void disableTimeoutLimit(){
        this.timeoutIsEnabled = false;
    }

    public void setTimeoutLimit(int timeoutLimit){
        this.timeoutLimit = timeoutLimit;
    }

    @Override // Strategy
    public void enable() {
        if(isEnabled) return;
        isEnabled = true;
        initialStateGPS = gps.isActive();
        initialStateGPSwithNetwork = gps.isNetworkActivated();

        if(debug) Toast.makeText(mainActivity.getApplicationContext(), " energyStrategy: " + isEnabled, Toast.LENGTH_SHORT).show();
        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: isEnabled = " + isEnabled);
    }

    @Override // Strategy
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        resetGPSToInitialState();
        if(debug) Toast.makeText(mainActivity.getApplicationContext(), " energyStrategy: " + isEnabled, Toast.LENGTH_SHORT).show();
        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: isEnabled = " + isEnabled);
    }
}
