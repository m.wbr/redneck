package strategies;

import android.location.Location;

public abstract class Strategy {

    protected boolean isEnabled = false;

    public abstract void enable();

    public abstract  void disable();

    public abstract void onLocationChanged(Location location);

    public boolean isEnabled(){
        return this.isEnabled;
    }

}
