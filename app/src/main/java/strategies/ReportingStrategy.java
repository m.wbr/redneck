package strategies;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import utility.FileHelper;
import utility.TimestampCreator;

public abstract class ReportingStrategy extends Strategy {

    private boolean debug = true;

    private int gpsFixCounter = 0;

    // execute when threshold is exceeded
    protected void onThresholdExceeded(JSONObject data){

        //countGPSFix();

        try {
            data.put("fix-count", getGPSFixCount());
            data.put("time", TimestampCreator.createTimestampString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendToServer(data);
    }

    protected void countGPSFix(){
        gpsFixCounter++;
    }

    public int getGPSFixCount() {
        return gpsFixCounter;
    }

    public void resetGPSFixCounter(){
        gpsFixCounter = 0;
    }

    private void sendToServer(JSONObject data) {

        if(this.debug) Log.e("ReportingStrategy", "DistanceBased: sendToServer()");

        // TODO: needs send method

        FileHelper.saveToFile(data.toString(), "strategie-log.txt", true);
        FileHelper.saveToFile(",", "strategie-log.txt", true);
    }
}
