package utility;

import android.location.Location;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONUtility {

    public  static String toTextView(Location location){

        double lat = location.getLatitude();
        double lon = location.getLongitude();
        double alt = location.getAltitude();
        String text = "GPS\n" + "Latitude: " + lat + "\n" + "Longitude: " + lon + "\n" + "Altitude " + alt;
        return text;
    }

    public static JSONObject toJSONObject(Location location){

        JSONObject data = new JSONObject();

        if(location == null){

            try {
                data.put("longitude", 0);
                data.put("latitude", 0);
                data.put("altitude", 0);
            }catch (JSONException e){
                Log.e("JSON EXCEPTION: ", String.valueOf(e));
            }
        }else{

            try {
                data.put("longitude", location.getLongitude());
                data.put("latitude", location.getLatitude());
                data.put("altitude", location.getAltitude());
            }catch (JSONException e){
                Log.e("JSON EXCEPTION: ", String.valueOf(e));
            }
        }
        return data;
    }
}
