package utility;

import com.example.referenceproject.MainActivity;

public class MainActivityHelper {

    private static MainActivity mainActivity;

    public static void setMainActivity(MainActivity activity){
        mainActivity = activity;
    }

    public static MainActivity getMainActivity(){
        return mainActivity;
    }
}
