package strategies;

import android.util.Log;
import org.json.JSONObject;

public abstract class ReportingStrategy extends Strategy {

    private boolean debug = true;
    private int gpsFixCounter = 0;

    // execute when threshold is exceeded
    protected void onThresholdExceeded(JSONObject data){
        countGPSFix();
        sendToServer();
    }

    private void countGPSFix(){
        gpsFixCounter++;
    }

    public int getGPSFixCount() {
        return gpsFixCounter;
    }

    public void resetGPSFixCounter(){
        gpsFixCounter = 0;
    }

    private void sendToServer() {

        if(this.debug) Log.e("ReportingStrategy", "DistanceBased: sendToServer()");

        // TODO: needs send method
    }
}
