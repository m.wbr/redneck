package strategies;

import android.location.Location;
import android.util.Log;
import org.json.JSONObject;

public class ReportingStrategyDistanceBased extends ReportingStrategy {

    private boolean debug = true;

    // Distance based reporting-strategy
    private float distanceThreshold = 0;        // in meter
    private float distance;                     // in meter
    private Location oldLocation = null;

    // check distance-threshold for reporting-strategy
    private boolean distanceThresholdExceeded(Location location){

        Location newLocation = location;
        if(oldLocation == null) oldLocation = newLocation;
        this.distance = this.oldLocation.distanceTo(newLocation);

        if(this.debug) Log.e("ReportingStrategy", "DistanceBased: distance = " + distance);

        if(distance > this.distanceThreshold){
            oldLocation = newLocation;
            return true;
        }else{
            return false;
        }
    }

    public void setDistanceThreshold(float meter) {
        this.distanceThreshold = meter;
    }

    public float getDistanceThreshold() {
        return this.distanceThreshold;
    }

    @Override // ReportingStrategy
    public void onLocationChanged(Location location) {

        if(!isEnabled) return;

        JSONObject data = new JSONObject(); // TODO: fill with data

        if(distanceThresholdExceeded(location)) this.onThresholdExceeded(data);
    }

    @Override // Strategy
    public void enable() {
        this.isEnabled = true;
    }

    @Override // Strategy
    public void disable() {
        this.isEnabled = false;
    }
}
