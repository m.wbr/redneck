package strategies;

import com.example.referenceproject.MainActivity;

import org.json.JSONObject;
import locationTracker.GPSLocationTracker;

public abstract class EnergyStrategy extends Strategy {

    protected MainActivity mainActivity;
    protected boolean initialStateGPS;            // used to set back the initial state of gps after energy-strategy is disabled
    protected boolean initialStateGPSwithNetwork;            // used to set back the initial state of gps after energy-strategy is disabled

    public EnergyStrategy(MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }

    protected GPSLocationTracker gps;

    public abstract void onSensorChanged(JSONObject data);

    public void setGPS(GPSLocationTracker gps){
        this.gps = gps;
    }

    // set
    protected void resetGPS(){

        if(initialStateGPS == gps.isActive()) return;

        if(initialStateGPS == false){
            gps.stopTracking();
            return;
        }

        if(initialStateGPSwithNetwork == true){
            gps.startTrackingWithNetwork();
            return;
        }

        gps.startTracking();

    }
}
