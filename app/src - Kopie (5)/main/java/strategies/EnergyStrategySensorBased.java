package strategies;

import android.location.Location;
import android.util.Log;

import com.example.referenceproject.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import sensors.Sensor;

public class EnergyStrategySensorBased extends EnergyStrategy{

    private boolean debug = true;
    private Sensor sensor;
    private boolean initialStateSensor;         // used to set back the initial state of sensor after energy-strategy is disabled
    private boolean gpsIsActive;
    private int accelarationThreshold = 2;
    private final double GRAVITY = 9.832;       // acceleration of GRAVITY

    public EnergyStrategySensorBased(MainActivity mainActivity, Sensor sensor){
        super(mainActivity);
        this.sensor = sensor;
        gpsIsActive = true;
    }

    @Override // EnergyStrategy
    public void onSensorChanged(JSONObject data) {

        if(!isEnabled) return;

        if(debug) Log.e("EnergyStrategy", "SensorBased: onSensorChanged()");

        if(gpsIsActive == true) return;

        if(phoneIsMoving(data)){

            if(debug) Log.e("EnergyStrategy", "SensorBased: phoneIsMoving() = " + true);

            if(initialStateGPSwithNetwork == true){
                gps.startTrackingWithNetwork();
            }else{
                gps.startTracking();
            }
            gpsIsActive = true;
            if(initialStateSensor == false) sensor.stop();
        }
    }

    @Override // EnergyStrategy
    public void onLocationChanged(Location location) {

        if(!isEnabled) return;

        if(debug) Log.e("EnergyStrategy", "SensorBased: onLocationChanged()");

        gps.stopTracking();
        gpsIsActive = false;

        if(debug) Log.e("EnergyStrategy", "SensorBased: initialStateSensor: " + initialStateSensor);

        if(initialStateSensor == false) sensor.start();

        if(debug) Log.e("EnergyStrategy", "SensorBased: sensorState: " + sensor.isActive());
    }

    private boolean phoneIsMoving(JSONObject data){

        if(debug) Log.e("EnergyStrategy", "SensorBased: phoneIsMoving()");

        // Beschleunigungen auf 0 setzen
        double x = 0;
        double y = 0;
        double z = 0;

        try{
            x = data.getDouble("x");
            y = data.getDouble("y");
            z = data.getDouble("z");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // filter gravity
        double totalAcceleration = Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2);
        totalAcceleration = Math.sqrt(totalAcceleration) - GRAVITY;

        String text = ""
                + "totalAcceleration = " + totalAcceleration
                + "\naccelarationThreshold = " + accelarationThreshold;

        mainActivity.strategyTextView.setText(text);

        if(this.debug) Log.e("EnergyStrategy", "SensorBased: x = " + x);
        if(this.debug) Log.e("EnergyStrategy", "SensorBased: y = " + y);
        if(this.debug) Log.e("EnergyStrategy", "SensorBased: z = " + z);
        if(this.debug) Log.e("EnergyStrategy", "SensorBased: totalAcceleration = " + totalAcceleration);
        if(this.debug) Log.e("EnergyStrategy", "SensorBased: accelarationThreshold = " + accelarationThreshold);

        if (totalAcceleration > accelarationThreshold){
            return true;
        }else{
            return false;
        }
    }

    @Override // Strategy
    public void enable() {
        this.isEnabled = true;
        initialStateSensor = sensor.isActive();
        initialStateGPS = gps.isActive();
        initialStateGPSwithNetwork = gps.isNetworkActivated();
    }

    @Override // Strategy
    public void disable() {

        this.isEnabled = false;

        resetSensor();
        resetGPS();
    }

    private void resetSensor(){

        if(initialStateSensor == sensor.isActive()) return;

        if(initialStateSensor == true){
            sensor.start();
        }else {
            sensor.stop();
        }
    }

    public void setSensor(Sensor sensor){
        this.sensor = sensor;
    }
}
