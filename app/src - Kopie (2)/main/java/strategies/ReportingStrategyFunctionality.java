package strategies;

public interface ReportingStrategyFunctionality {

    /**
     * send GPS-fix to server
     */
    public void sendToServer();

    /**
     * increment GPS-fix counter
     */
    public void countGPSFix();

    /**
     * reset GPS-Fix counter
     */
    public void resetGPSFixCounter();

    /**
     * get GPS-Fix counter
     * @return int
     */
    public int getGPSFixCount();
}
