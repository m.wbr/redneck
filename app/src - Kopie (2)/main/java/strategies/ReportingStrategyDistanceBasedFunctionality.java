package strategies;

import android.location.Location;
import android.location.LocationListener;

public interface ReportingStrategyDistanceBasedFunctionality {

    public void setDistanceThreshold(float meter);

    public float getDistanceThreshold();
}
