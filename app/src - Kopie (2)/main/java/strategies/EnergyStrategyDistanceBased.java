package strategies;

import android.location.Location;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import utility.MainActivityHelper;

public class EnergyStrategyDistanceBased extends EnergyStrategy {

    // debug stuff
    private boolean debug = true;
    private int counter = 0;

    private Location oldLocation;
    private float estimatedDistance;
    private float distanceGPS;
    private float distanceThreshold = 5;
    private double velocity;                 // in meter/s
    private float maxVelocity = 0.5f;
    private double time = -1;
    private double deltaTime;
    private double timeAtLastFix = -1;
    private double timeBetweenFixes = 0;
    private boolean energyPlanIsActive = false;
    private boolean gpsIsRunning = true;

    // safety threshold
    private boolean safetyThresholdIsEnabled = true;
    private int intervalCounter = 0;
    private int intervalLimit = 10;

    // thread stuff
    private Thread thread;
    private int sleepInterval = 2000;      // in milliseconds

    public EnergyStrategyDistanceBased(){
        reset();
    }

    private void reset(){
        intervalCounter = 0;
        estimatedDistance = 0;
    }

    @Override // EnergyStrategy
    public void onSensorChanged(JSONObject data) {

        // TODO: needs code
    }

    @Override // LocationListener
    public void onLocationChanged(Location location) {

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: onLocationChanged()");

        MainActivityHelper.getMainActivity().textView.append("\nenergySrategyisEnabled = " + isEnabled + "\n");

        if(!isEnabled) return;

        calculateDeltaTime();
        calculateGPSDistance(location);
        calculateTimeBetweenFixes();
        calculateVelocity();
        calculateEnergyPlan();
    }

    private void calculateDeltaTime(){

        double newTime = (double)System.currentTimeMillis()/1000;
        if(this.time == -1){
            deltaTime = 0;
        }else{
            deltaTime =  newTime - time;
        }
        time = newTime;

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: calculateDeltaTime() = " + deltaTime);
    }

    private void calculateGPSDistance(Location location){

        Location newLocation = location;
        if(oldLocation == null){
            distanceGPS = 0;
        }else{
            distanceGPS = this.oldLocation.distanceTo(newLocation);
        }
        oldLocation = newLocation;

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: calculateGPSDistance() = " + distanceGPS);
    }

    private void calculateTimeBetweenFixes(){

        double timeAtNewFix = (double)System.currentTimeMillis()/1000;

        if(timeAtLastFix != -1) timeBetweenFixes = timeAtNewFix - timeAtLastFix;

        timeAtLastFix = timeAtNewFix;

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: calculateTimeBetweenFixes() = " + timeBetweenFixes);
    }

    private void calculateVelocity(){

        if(timeBetweenFixes == 0 || distanceGPS == 0){
            velocity = 0;
        }else{
            velocity = distanceGPS / timeBetweenFixes;
        }
        if(velocity > maxVelocity) velocity = maxVelocity;

        if(debug) Log.e("EnergyStrategy", "calculateVelocity() = " + velocity);
    }

    private void calculateEnergyPlan(){

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: calculateEnergyPlan().start");

        if(distanceGPS == 0) return;
        if(velocity == 0) return;
        if(deltaTime == 0) return;
        if(timeBetweenFixes == 0) return;

        startEnergyPlan();

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: calculateEnergyPlan().end");
    }

    private void startEnergyPlan(){

        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: startEnergyPlan()");

        if(thread == null || !thread.isAlive()) {
            thread = new Thread() {
                @Override
                public void run() {

                    Looper.prepare();
                    
                    energyPlanIsActive = true;
                    gps.stopTracking();
                    gpsIsRunning = false;

                    while (energyPlanIsActive) {

                        try {
                            sleep(sleepInterval);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (debug) Log.e("Thread running?", String.valueOf(thread.isAlive()));

                        MainActivityHelper.getMainActivity().runOnUiThread(showEnergyPlan);
                        executeEnergyPlan();

                    }
                    MainActivityHelper.getMainActivity().runOnUiThread(terminateEnergyPlan);
                    if (debug) Log.e("Thread running?", String.valueOf(thread.isAlive()));
                }
            };
            thread.start();
        }
    }

    private void executeEnergyPlan(){

        calculateDeltaTime();
        estimatedDistance += velocity * deltaTime;

        if(debug) Log.e("EnergyStrategy", "velocity: " + velocity);
        if(debug) Log.e("EnergyStrategy", "velocity*deltaTime: " + (velocity * deltaTime));
        if(debug) Log.e("EnergyStrategy", "deltaTime: " + deltaTime);
        if(debug) Log.e("EnergyStrategy", "estimatedDistance: " + estimatedDistance);
        if(debug) Log.e("EnergyStrategy", "distanceThreshold: " + distanceThreshold);

        if(safetyThresholdIsEnabled){
            intervalCounter++;
            if(debug) Log.e("EnergyStrategy", "DistanceBased: intervalCounter = " + intervalCounter);
            if(debug) Log.e("EnergyStrategy", "DistanceBased: intervalLimit = " + intervalLimit);
            if(intervalCounter >= intervalLimit) energyPlanIsActive = false;
        }

        if(estimatedDistance >= distanceThreshold) energyPlanIsActive = false;
    }

    private Runnable showEnergyPlan = new Runnable() {
        @Override
        public void run() {

            String text = "Energy Strategy"
                    + "\nvelocity = " + velocity
                    + "\nmaxVelocity = " + maxVelocity
                    + "\nestimatedDistance = " + estimatedDistance
                    + "\ndistanceThreshold = " + distanceThreshold
                    + "\ndistance last fix = " + distanceGPS
                    + "\nTimeDifference = " + deltaTime
                    + "\ncounter = " + counter
                    + "\nintervalCounter = " + intervalCounter
                    + "\nintervalLimit = " + intervalLimit;

            MainActivityHelper.getMainActivity().strategyTextView.setText(text);
        }
    };

    private Runnable terminateEnergyPlan = new Runnable() {
        @Override
        public void run() {

            stopEnergyPlan();
        }
    };

    private void stopEnergyPlan(){

        if(thread != null){

            if(this.debug) Log.e("EnergyStrategy", "DistanceBased: stopEnergyPlan()");
            if(debug) Log.e("stopEnergyPlan: Thread", String.valueOf(thread.isAlive()));

            try {
                thread.join();
                if(debug) Log.e("afterJoin: Thread", String.valueOf(thread.isAlive()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            thread.interrupt();

            if(debug) Log.e("afterInterrupt: Thread", String.valueOf(thread.isAlive()));
        }

        reset();
        counter++;
        if(!gpsIsRunning) gps.startTracking();
        gpsIsRunning = true;
    }

    public void setDistanceThreshold(float distanceThreshold){
        this.distanceThreshold = distanceThreshold;
    }

    @Override // Strategy
    public void enable() {
        isEnabled = true;
        if(debug) Toast.makeText(MainActivityHelper.getMainActivity().getApplicationContext(), " energyStrategy: " + isEnabled, Toast.LENGTH_SHORT).show();
        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: isEnabled = " + isEnabled);
    }

    @Override // Strategy
    public void disable() {
        isEnabled = false;
        stopEnergyPlan();
        if(debug) Toast.makeText(MainActivityHelper.getMainActivity().getApplicationContext(), " energyStrategy: " + isEnabled, Toast.LENGTH_SHORT).show();
        if(this.debug) Log.e("EnergyStrategy", "DistanceBased: isEnabled = " + isEnabled);
    }
}
