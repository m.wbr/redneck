package strategies;

public abstract class Strategy {

    protected boolean isEnabled = false;

    public abstract void enable();

    public abstract  void disable();

    public boolean isEnabled(){
        return this.isEnabled;
    }
}
