package com.example.referenceproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        TextView counterTextView = (TextView)findViewById(R.id.counterTextView);

        if(getIntent().hasExtra("counter")){

            String counterString = Integer.toString(getIntent().getIntExtra("counter", 0));

            counterTextView.setText(counterString);
        }

    }
}
