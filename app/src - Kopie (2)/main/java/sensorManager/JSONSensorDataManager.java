package sensorManager;

import android.util.Log;
import android.widget.TextView;

import com.example.referenceproject.MainActivity;
import com.example.referenceproject.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import utility.TimestampCreator;

public class JSONSensorDataManager{

    private List<JSONSensorData> sensorList;
    private MainActivity mainActivity;
    private boolean keepRunning;
    private Thread thread;
    private int interval = 1000;
    private boolean debug = true;

    public JSONSensorDataManager(MainActivity mainActivity){

        this.mainActivity = mainActivity;
        sensorList = new LinkedList<>();
    }

    private Runnable loggingRunnable = new Runnable() {
        @Override
        public void run() {

            TextView text = mainActivity.findViewById(R.id.gpsTextView);
            text.setText(getData().toString());
        }
    };

    public void startLogging(){

        if(thread == null || !thread.isAlive()){

            thread = new Thread(){
              @Override
              public void run(){
                  keepRunning = true;

                  while(keepRunning){

                      mainActivity.runOnUiThread(loggingRunnable);
                      try {
                          sleep(interval);
                      } catch (InterruptedException e) {
                          e.printStackTrace();
                      }
                      if(debug) Log.e("Thread running?", String.valueOf(thread.isAlive()));
                  }
                  if(debug) Log.e("Thread running?", String.valueOf(thread.isAlive()));
              }
            };
            thread.start();
        }
    }

    public void stopLogging(){

        keepRunning = false;

        if(debug) Log.e("Thread running?", String.valueOf(thread.isAlive()));
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread.interrupt();

        if(debug) Log.e("Thread running?", String.valueOf(thread.isAlive()));
    }

    /**
     * adds a sensor with interface: JSONSensorData
     * @param sensor
     */
    public void addSensor(JSONSensorData sensor){

        if(!sensorList.contains(sensor)){
            this.sensorList.add(sensor);
            if(debug) Log.e("addSensor", "new sensor");
        }else{
            if(debug) Log.e("addSensor", "sensor already in list");
        }

        if(debug) Log.i("sensor List size = ", ""+this.sensorList.size());
    }

    /**
     * removes a sensor
     * @param sensor
     */
    public void removeSensor(JSONSensorData sensor){

        this.sensorList.remove(sensor);
    }

    /**
     * returns a single JSON object with all data of all added sensors
     * @return JSONObject
     */
    public JSONObject getData(){

        JSONObject data = new JSONObject();

        try {
            data.put("timestamp", TimestampCreator.createTimestampString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(JSONSensorData jsonData : this.sensorList){
            try {
                // JSON={SensorName, Data}
                data.put(jsonData.getSensorName(), jsonData.getData());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }

    public void setInterval(int interval){
        this.interval = interval;
    }

    public int getInterval(){
        return this.interval;
    }
}
