package com.example.referenceproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import utility.MainActivityHelper;
import locationTracker.GPSFusedLocationTracker;
import locationTracker.GPSLocationTracker;
import sensorManager.JSONSensorDataManager;
import sensors.Accelerometer;
import sensors.Gyroscope;
import sensors.Magnetometer;
import sensors.Sensor;
import strategies.EnergyStrategy;
import strategies.EnergyStrategyDistanceBased;
import strategies.ReportingStrategy;
import strategies.ReportingStrategyDistanceBased;
import strategies.ReportingStrategyTimeBased;
import strategies.Strategy;

public class MainActivity extends AppCompatActivity {

    Button activateSensorsButton;
    Button deactivateSensorsButton;;
    Button getSensorDataButton;

    Button activateGPSButton;
    Button activateGPSWithNetworkButton;
    Button deactivateGPSButton;
    Button activateFusedGPSHighButton;
    Button activateFusedGPSNormalButton;
    Button deactivateFusedGPSButton;
    Button activateEnergyPlanButton;
    Button deactivateEnergyPlanButton;

    public TextView textView;
    public TextView strategyTextView;

    private boolean keepRunning = false;

    private JSONSensorDataManager sensorDataManager;

    private Sensor accelerometer;
    private Sensor gyroscope;
    private Sensor magnetometer;
    private GPSLocationTracker gps;
    private GPSFusedLocationTracker gpsFused;
    private ReportingStrategy reportingStrategy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivityHelper.setMainActivity(this);

        activateSensorsButton = (Button) findViewById(R.id.activateSensorsButton);
        deactivateSensorsButton = (Button) findViewById(R.id.deactivateSensorsButton);

        activateGPSButton = (Button) findViewById(R.id.gpsOnButton);
        activateGPSWithNetworkButton = (Button) findViewById(R.id.gpsWithNetworkOnButton);
        deactivateGPSButton = (Button) findViewById(R.id.gpsOffButton);

        activateFusedGPSHighButton = (Button) findViewById(R.id.fusedGpsOnButtonHigh);
        activateFusedGPSNormalButton = (Button) findViewById(R.id.fusedGpsOnButtonNormal);
        deactivateFusedGPSButton = (Button) findViewById(R.id.fusedGpsOffButton);

        activateEnergyPlanButton = (Button) findViewById(R.id.energyPlanOnButton);
        deactivateEnergyPlanButton = (Button) findViewById(R.id.energyPlanOffButton);

        getSensorDataButton = (Button) findViewById(R.id.getSensorDataButton);

        textView = (TextView) findViewById(R.id.gpsTextView);
        strategyTextView = (TextView) findViewById(R.id.strategyTextView);

        // create manager
        sensorDataManager = new JSONSensorDataManager(this);

        // create sensors
        final EnergyStrategy energyStrategy = new EnergyStrategyDistanceBased(this);
        //energyStrategy.enable();

        accelerometer = new Accelerometer(MainActivity.this);
        gyroscope = new Gyroscope(MainActivity.this);
        magnetometer = new Magnetometer(MainActivity.this);

        // reporting-strategy part
        reportingStrategy = new ReportingStrategyTimeBased();
        //reportingStrategy = new ReportingStrategyDistanceBased();

        // create GPS
        gps = new GPSLocationTracker(MainActivity.this);
        gps.setEnergyStrategy(energyStrategy);
        //gps.startTracking();

        gpsFused = new GPSFusedLocationTracker((MainActivity.this));

        activateGPSButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                gps.startTracking();
            }
        });

        activateGPSWithNetworkButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                gps.startTrackingWithNetwork();
            }
        });

        deactivateGPSButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                gps.stopTracking();
            }
        });

        activateFusedGPSHighButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                gpsFused.setHighAccuracy();
                gpsFused.startTracking();
            }
        });

        activateFusedGPSNormalButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                gpsFused.setBalancedAccuracy();
                gpsFused.startTracking();
            }
        });

        deactivateFusedGPSButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                gpsFused.stopTracking();
            }
        });

        activateEnergyPlanButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                energyStrategy.enable();
            }
        });

        deactivateEnergyPlanButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                energyStrategy.disable();
            }
        });

        getSensorDataButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //FileHelper.saveToFile("Test");


                //sensorDataManager.startLogging();


                /*
                JSONObject json = sensorDataManager.getData();

                String data = "";

                data += sensorDataManager.getData().toString();
                */
                /*
                try {
                    data += json.get("Magnetometer").toString();
                    data += "\n";
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    data += json.get("Gyroscope").toString();
                    data += "\n";
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    data += json.get("Accelerometer").toString();
                    data += "\n";
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                 */

                //gpsTextView.setText("" + data);
            }
        });

        activateSensorsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //gpsFused.startTracking();

                /*
                accelerometer.start();
                gyroscope.start();
                magnetometer.start();
                gps.startTrackingWithNetwork();

                sensorDataManager.addSensor(accelerometer);
                sensorDataManager.addSensor(gyroscope);
                sensorDataManager.addSensor(magnetometer);
                sensorDataManager.addSensor(gps);
                */
            }
        });

        deactivateSensorsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //gpsFused.stopTracking();

                /*
                sensorDataManager.stopLogging();

                sensorDataManager.removeSensor(accelerometer);
                sensorDataManager.removeSensor(gyroscope);
                sensorDataManager.removeSensor(magnetometer);
                sensorDataManager.removeSensor(gps);

                accelerometer.stop();
                gyroscope.stop();
                magnetometer.stop();
                gps.stopTracking();

                 */

            }
        });

    }
}
