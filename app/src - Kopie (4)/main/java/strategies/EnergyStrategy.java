package strategies;

import com.example.referenceproject.MainActivity;

import org.json.JSONObject;
import locationTracker.GPSLocationTracker;

public abstract class EnergyStrategy extends Strategy {

    protected MainActivity mainActivity;

    public EnergyStrategy(MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }

    protected GPSLocationTracker gps;

    public abstract void onSensorChanged(JSONObject data);

    public void setGPS(GPSLocationTracker gps){
        this.gps = gps;
    }
}
