package locationTracker;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.referenceproject.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import utility.FileHelper;
import utility.TimestampCreator;
import sensorManager.JSONSensorData;

public class GPSLocationTracker implements LocationListener, JSONSensorData {

    private LocationManager locationManager;
    protected MainActivity mainActivity;
    private Calendar calendar;
    private long timeThreshold = 1000;
    private float distanceThreshold = 0;
    private JSONObject data;
    protected Location location;
    protected boolean debug = false;

    public GPSLocationTracker(MainActivity mainActivity){ //, onLocationChangeListener onLocationChangeListener){

        this.mainActivity = mainActivity;
        locationManager = (LocationManager) mainActivity.getSystemService(Context.LOCATION_SERVICE);
        calendar = Calendar.getInstance();
    }

    /**
     * Starts location tracking only with GPS provider
     */
    public void startTracking() {
        Toast.makeText(mainActivity.getApplicationContext(), "starting GPS", Toast.LENGTH_LONG).show();
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, timeThreshold, distanceThreshold, this);
            if(debug) Log.e("GPSLocationTracker", "Logger Started");

            //changeListener.setLogRecord("GPS tracking started");
        }
        catch(SecurityException e) {
            e.printStackTrace();
            //changeListener.setLogRecord("GPS tracking failed");
        }
    }

    /**
     * Starts location tracking with network provider
     */
    public void startTrackingWithNetwork() {
        Toast.makeText(mainActivity.getApplicationContext(), "starting GPS with network", Toast.LENGTH_LONG).show();
        try {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, timeThreshold, distanceThreshold, this);
            //changeListener.setLogRecord("Network tracking started");
        }
        catch(SecurityException e) {
            e.printStackTrace();
            //changeListener.setLogRecord("Network tracking failed");

        }
    }

    /**
     * Stops tracking location
     */
    public void stopTracking() {
        Toast.makeText(mainActivity.getApplicationContext(), "stopping GPS", Toast.LENGTH_LONG).show();
        locationManager.removeUpdates(this);
        //changeListener.setLogRecord("Tracking stopped");
    }

    /**
     * Threshold for location updates. Only locations that pass threshold will be added to location list.
     * @param timeThreshold time in milliseconds
     */
    public void setTrackingTimeThreshold(long timeThreshold){
        this.timeThreshold = timeThreshold;
    }

    /**
     * Threshold for location updates. Only locations that pass threshold will be added to location list.
     * @return timeThreshold in milliseconds
     */
    public long getTrackingTimeThreshold(){
        return this.timeThreshold;
    }

    /**
     * Threshold for location updates. Only locations that pass threshold will be added to location list.
     * @param distanceThreshold in meters
     */
    public void setTrackingDistanceThreshold(float distanceThreshold){
        this.distanceThreshold = distanceThreshold;
    }

    /**
     * Threshold for location updates. Only locations that pass threshold will be added to location list.
     * @return distanceThreshold in meters
     */
    public float getTrackingDistanceThreshold(){
        return this.distanceThreshold;
    }

    ///////////////////////////////////////////// INTERFACES /////////////////////////////////////////////

    @Override // LocationListener
    public void onLocationChanged(Location location) {

        this.location = location;

        //Toast.makeText(mainActivity.getApplicationContext(), "locationChanged", Toast.LENGTH_LONG).show();
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        double alt = location.getAltitude();
        String text = "GPS\n" + "Latitude: " + lat + "\n" + "Longitude: " + lon + "\n" + "Altitude " + alt;
        mainActivity.textView.setText(text);

        JSONObject jsonData = this.getData();
        try {
            jsonData.put("time", TimestampCreator.createTimestampString());
            jsonData.put("type:", "GPS");
        }catch (JSONException e){
            Log.e("JSON EXCEPTION: ", String.valueOf(e));
        }
        String data = jsonData.toString();

        FileHelper.saveToFile(data + ",");
    }

    @Override // LocationListener
    public void onProviderDisabled(String provider) {
        Toast.makeText(mainActivity.getApplicationContext(), "Please Activate " + provider.toUpperCase(), Toast.LENGTH_LONG).show();
    }

    @Override // LocationListener
    public void onStatusChanged(String provider, int status, Bundle extras) {
        if(debug) Toast.makeText(mainActivity.getApplicationContext(), provider.toUpperCase() + " HAS CHANGED\n" + "STATUS: " + status, Toast.LENGTH_LONG).show();
    }

    @Override // LocationListener
    public void onProviderEnabled(String provider) {
        if(debug) Toast.makeText(mainActivity.getApplicationContext(), provider.toUpperCase() + " ENABLED", Toast.LENGTH_LONG).show();
    }

    @Override // JSONSensorData
    public JSONObject getData() {

        data = new JSONObject();

        if(location == null){

            if(debug) Toast.makeText(mainActivity.getApplicationContext(), "gps location == null", Toast.LENGTH_LONG).show();
            try {
                data.put("longitude", 0);
                data.put("latitude", 0);
                data.put("altitude", 0);
            }catch (JSONException e){
                if(debug) Toast.makeText(mainActivity.getApplicationContext(), "JSONException GPSLocationManager", Toast.LENGTH_LONG).show();
                Log.e("JSON EXCEPTION: ", String.valueOf(e));
            }
        }else{

            try {
                data.put("longitude", location.getLongitude());
                data.put("latitude", location.getLatitude());
                data.put("altitude", location.getAltitude());
            }catch (JSONException e){
                if(debug) Toast.makeText(mainActivity.getApplicationContext(), "JSONException GPSLocationManger", Toast.LENGTH_LONG).show();
                Log.e("JSON EXCEPTION: ", String.valueOf(e));
            }
        }
        return data;
    }

    @Override // JSONSensorData
    public String getSensorName() {
        return "GPS";
    }
}