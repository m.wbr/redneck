package strategies;

import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.example.referenceproject.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import utility.TimestampCreator;

public class ReportingStrategyDistanceBasedGPS extends ReportingStrategy implements ReportingStrategyDistanceBasedFunctionality{

    private float distanceThreshold = 0;
    private double distance;
    private Location oldLocation = null;

    public ReportingStrategyDistanceBasedGPS(MainActivity mainActivity){
        super(mainActivity);
        this.debug = true;
        this.setTrackingDistanceThreshold(0);   // set threshold of GPSLocationTracker to 0 to avoid interference with reporting-strategy
    }

    @Override // ReportingStrategy
    void onThresholdExceeded() {
        countGPSFix();
        sendToServer();
    }

    @Override // ReportingStrategy
    public void sendToServer() {
        if(this.debug) Toast.makeText(this.mainActivity.getApplicationContext(), "ReportingStrategyDistanceBased: sendToServer()", Toast.LENGTH_LONG).show();
        if(this.debug) Log.e("ReportingStrategy", "DistanceBased: sendToServer()");

        // TODO: needs send method
    }

    @Override // ReportingStrategyDistanceBasedFunctionality
    public void setDistanceThreshold(float meter) {
        this.distanceThreshold = meter;
    }

    @Override // ReportingStrategyDistanceBasedFunctionality
    public float getDistanceThreshold() {
        return this.distanceThreshold;
    }

    @Override // ReportingStrategy (from parent class: GPSLocationTracker)
    public void onLocationChanged(Location location) {

        if(!thresholdExceeded(location)) return;

        this.location = location;

        double lat = location.getLatitude();
        double lon = location.getLongitude();
        double alt = location.getAltitude();
        int gpsFixCounter = this.getGPSFixCount();
        String text = "GPS\n" + "Latitude: " + lat + "\nLongitude: " + lon + "\nAltitude " + alt + "\nGPSFixCounterDistanceBased: " + gpsFixCounter + "\nnewDistance: " + this.distance;
        mainActivity.textView.setText(text);

        JSONObject jsonData = this.getData();
        try {
            jsonData.put("time", TimestampCreator.createTimestampString());
            jsonData.put("type:", "GPS");
        }catch (JSONException e){
            Log.e("JSON EXCEPTION: ", String.valueOf(e));
        }
        String data = jsonData.toString();

        onThresholdExceeded();
    }

    private boolean thresholdExceeded(Location newLocation){

        if(oldLocation == null) oldLocation = newLocation;

        this.distance = this.oldLocation.distanceTo(newLocation);

        if(this.debug) Log.e("ReportingStrategy", "DistanceBased: distance = " + distance);

        if(distance > this.distanceThreshold){
            oldLocation = newLocation;
            return true;
        }else{
            return false;
        }
    }
}
