package strategies;

import android.location.Location;
import android.util.Log;
import android.widget.Toast;
import com.example.referenceproject.MainActivity;
import org.json.JSONException;
import org.json.JSONObject;

import utility.TimestampCreator;
import utility.FileHelper;

public class ReportingStrategyTimeBasedGPS extends ReportingStrategy implements ReportingStrategyTimeBasedFunctionality {

    long time;
    long timeThreshold = 1;
    long timeDifference;

    public ReportingStrategyTimeBasedGPS(MainActivity mainActivity){
        super(mainActivity);
        this.debug = true;
        this.setTrackingTimeThreshold(0);
    }

    @Override // GPSLocationTracker (parent class)
    public void startTracking(){
        super.startTracking();
        this.time = TimestampCreator.createTimestampNumber();
    }

    @Override // ReportingStrategy
    void onThresholdExceeded() {

        countGPSFix();
        sendToServer();
        String data = "" + this.getGPSFixCount();
        FileHelper.saveToFile(data, "count.txt", false);
    }

    @Override // ReportingStrategyTimeBasedFunctionality
    public void setTimeThreshold(long seconds) {

        // TODO: use locationManager threshold or use own implementation?
        this.timeThreshold = seconds;
    }

    @Override // ReportingStrategyTimeBasedFunctionality
    public long getTimeThreshold() {
        return this.getTrackingTimeThreshold();
    }

    @Override // ReportingStrategy
    public void sendToServer() {
        if(this.debug) Toast.makeText(this.mainActivity.getApplicationContext(), "ReportingStrategyTimeBased: sendToServer()", Toast.LENGTH_LONG).show();
        if(this.debug) Log.e("ReportingStrategy", "TimeBased: sendToServer()");

        // TODO: needs send method
    }

    @Override // ReportingStrategy (from parent class: GPSLocationTracker)
    public void onLocationChanged(Location location) {

        if(!thresholdExceeded()) return;

        this.location = location;

        double lat = location.getLatitude();
        double lon = location.getLongitude();
        double alt = location.getAltitude();
        int gpsFixCounter = this.getGPSFixCount();
        String text = "GPS\n" + "Latitude: " + lat + "\nLongitude: " + lon + "\nAltitude " + alt + "\nGPSFixCounterTimeBased: " + gpsFixCounter + "\nTime: " + this.time + "\nTimeDiff: " + timeDifference;
        mainActivity.textView.setText(text);

        JSONObject jsonData = this.getData();
        try {
            jsonData.put("time", TimestampCreator.createTimestampString());
            jsonData.put("type:", "GPS");
        }catch (JSONException e){
            Log.e("JSON EXCEPTION: ", String.valueOf(e));
        }
        String data = jsonData.toString();

        onThresholdExceeded();
    }

    private boolean thresholdExceeded(){

        long newTime = TimestampCreator.createTimestampNumber();
        this.timeDifference =  newTime - this.time;

        if(timeDifference < this.timeThreshold) return false; // TODO: use locationManager threshold or use own implementation?

        this.time = newTime;

        return true;
    }
}
