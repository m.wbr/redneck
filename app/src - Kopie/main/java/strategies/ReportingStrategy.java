package strategies;

import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.example.referenceproject.MainActivity;

import locationTracker.GPSLocationTracker;

public abstract class ReportingStrategy extends GPSLocationTracker implements ReportingStrategyFunctionality{

    private boolean isEnabled = true;
    private int gpsFixCounter = 0;

    public ReportingStrategy(MainActivity mainActivity){
        super(mainActivity);
    }

    abstract void onThresholdExceeded();
    public abstract void onLocationChanged(Location location);

    @Override // ReportingStrategyFunctionality
    public void countGPSFix(){
        gpsFixCounter++;
    }

    @Override // ReportingStrategyFunctionality
    public int getGPSFixCount() {
        return gpsFixCounter;
    }

    @Override // ReportingStrategyFunctionality
    public void resetGPSFixCounter(){
        gpsFixCounter = 0;
    }

    @Override // ReportingStrategyFunctionality
    public abstract void sendToServer();
}
