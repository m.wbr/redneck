package strategies;

import android.location.Location;
import android.location.LocationListener;

public interface ReportingStrategyTimeBasedFunctionality {

    /**
     * set time-intervall for GPS-fixes
     * @param milliSeconds
     */
    public void setTimeThreshold(long milliSeconds);

    /**
     * get time-intervall for GPS-fixes
     * @return time in milliSeconds: long
     */
    public long getTimeThreshold();
}
