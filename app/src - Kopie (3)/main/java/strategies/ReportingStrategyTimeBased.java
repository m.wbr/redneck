package strategies;

import android.location.Location;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import com.example.referenceproject.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import utility.MainActivityHelper;
import utility.TimestampCreator;
import utility.FileHelper;



public class ReportingStrategyTimeBased extends ReportingStrategy implements ReportingStrategyTimeBasedFunctionality {

    private boolean debug = true;

    // Time based strategy
    private double time = -1;
    private long timeThreshold = 5;             // in seconds
    private double timeDifference;

    // check time-threshold for reporting-strategy
    private boolean timeThresholdExceeded(){

        double newTime = (double)System.currentTimeMillis()/1000;
        if(this.time == -1) this.time = newTime;
        this.timeDifference = newTime - this.time;

        if(debug) Log.e("ReportingStrategy", "TimeBased: deltaTime = " + time);

        if(timeDifference < this.timeThreshold){
            return false;
        }else{
            this.time = newTime;
            return true;
        }
    }

    @Override // ReportingStrategyTimeBasedFunctionality
    public void setTimeThreshold(long seconds) {
        this.timeThreshold = seconds;
    }

    @Override // ReportingStrategyTimeBasedFunctionality
    public long getTimeThreshold() {
        return this.timeThreshold;
    }

    @Override // ReportingStrategy
    public void onLocationChanged(Location location) {

        if(!isEnabled) return;

        // TODO: fill with data
        JSONObject data = new JSONObject();

        if(timeThresholdExceeded()){

            String text = "\nTimeDiff: " + this.timeDifference + "\nCounter: " + this.getGPSFixCount();

            onThresholdExceeded(data);
            MainActivity mainActivity = MainActivityHelper.getMainActivity();
            mainActivity.textView.append(text);
        }
    }

    @Override // Strategy
    public void enable() {
        this.isEnabled = true;
    }

    @Override // Strategy
    public void disable() {
        this.isEnabled = false;
    }
}

