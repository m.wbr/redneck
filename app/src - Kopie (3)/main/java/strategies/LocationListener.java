package strategies;

import android.location.Location;

public interface LocationListener {

    public void onLocationChanged(Location location);
}
