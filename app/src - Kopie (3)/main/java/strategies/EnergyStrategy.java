package strategies;

import android.location.Location;

import org.json.JSONObject;

import locationTracker.GPSLocationTracker;

public abstract class EnergyStrategy extends Strategy implements LocationListener{

    protected GPSLocationTracker gps;

    public abstract void onSensorChanged(JSONObject data);

    public void setGPS(GPSLocationTracker gps){
        this.gps = gps;
    }
}
