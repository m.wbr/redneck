package strategies;

import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.example.referenceproject.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import utility.FileHelper;
import utility.MainActivityHelper;
import utility.TimestampCreator;
import locationTracker.GPSLocationTracker;

public abstract class ReportingStrategy extends Strategy implements ReportingStrategyFunctionality, LocationListener {

    private boolean debug = true;
    private int gpsFixCounter = 0;

    // execute when threshold is exceeded
    protected void onThresholdExceeded(JSONObject data){
        countGPSFix();
        sendToServer();
    }

    @Override // ReportingStrategyFunctionality
    public void countGPSFix(){
        gpsFixCounter++;
    }

    @Override // ReportingStrategyFunctionality
    public int getGPSFixCount() {
        return gpsFixCounter;
    }

    @Override // ReportingStrategyFunctionality
    public void resetGPSFixCounter(){
        gpsFixCounter = 0;
    }

    @Override // ReportingStrategyFunctionality
    public void sendToServer() {
        if(this.debug) Log.e("ReportingStrategy", "DistanceBased: sendToServer()");

        String data = "gps fix count: " + this.getGPSFixCount();
        FileHelper.saveToFile("counter.txt", data, false);

        // TODO: needs send method
    }
}
